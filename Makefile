CC=g++
CFLAGS=-I.
CXXFLAGS=-std=c++11 -DDIF_VERBOSE=0 -g3 -O3
COMMON_OBJ=difcodec.o

all: encoder decoder
	./encoder
	./decoder

test: unit_test
	./unit_test

unit_test: unit_test.o $(COMMON_OBJ)

encoder: encoder.o lodepng.o $(COMMON_OBJ)
	$(CC) -o $@ $^

decoder: decoder.o lodepng.o $(COMMON_OBJ)
	$(CC) -o $@ $^

clean:
	rm -f *.o
	rm -f encoder decoder unit_test
