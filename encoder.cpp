#include "lodepng.h"
#include "difcodec.h"
#include <iostream>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <fstream>

size_t get_image_data_size(uint32_t width, uint32_t height) {
    return width * height * 4 /* bytes per pixel: RGBA */;
}

bool compress(const char* inputFilename, const char* outputFilename, int tolerance) {
    uint32_t error;
    uint8_t* image;
    uint32_t width, height;

    // Load png file as a raw vector
    error = lodepng_decode32_file(&image, &width, &height, inputFilename);
    if (error) {
        printf("error %u: %s\n", error, lodepng_error_text(error));
        return false;
    }
    std::vector<uint8_t> buffer(image, image + get_image_data_size(width, height));
    free(image);

    // Encode the image with the DIF codec
    DIFCodec codec{tolerance};
    bool ok;
    buffer = codec.encode(buffer, width, height, &ok);
    if (!ok) {
        return false;
    }

    // Write encoded image data
    std::ofstream outfile;
    outfile.open(outputFilename);
    outfile.write((char *)buffer.data(), buffer.size());
    outfile.close();

    return true;
}

void usage(int argc, char *argv[]) {
    printf("Usage:\n");
    printf("\t%s [flags] [input.png] [output.bin]\n", argv[0]);
    printf("Flags:\n");
    printf("\t-t, --tolerance <num>\n");
    printf(
            "\t   Color tolerance: colors, whose numeric difference is less "
            "than the given number, will be considered the same. "
            "The difference is calculated as follows: differences of all channels "
            "are calculated separately (each channel value is from 0 to 255), "
            "and then summed up. "
            "Default: 50.\n");
    printf("\t-h, --help\n");
    printf("\t   Prints this message.\n");
}

int main(int argc, char *argv[]) {
    int argnum = 1;
    int tolerance = -1;

    for (; argnum < argc && argv[argnum][0] == '-'; argnum++) {
        if (strcmp(argv[argnum], "-t") == 0 || strcmp(argv[argnum], "--tolerance") == 0) {
            argnum++;
            tolerance = strtol(argv[argnum], nullptr, 10);
        } else if (strcmp(argv[argnum], "-h") == 0 || strcmp(argv[argnum], "--help") == 0) {
            usage(argc, argv);
            return 0;
        }
    }

    const char* inputFilename = argc > argnum ? argv[argnum] : "test.png";
    argnum++;

    const char* outputFilename = argc > argnum ? argv[argnum] : "output.bin";
    argnum++;

    bool ok = compress(inputFilename, outputFilename, tolerance);
    if (!ok) {
        return 1;
    }
    return 0;
}
