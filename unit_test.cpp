/*
 * Unit tests for DIF codec
 */

#include "difcodec.h"
#include <cstdio>

#define FAIL(str, line)                           \
    do {                                            \
        printf("Fail on line %d: [%s]\n", line, str); \
        return str;                                   \
    } while (0)

#define ASSERT(expr)                    \
    do {                                  \
        static_num_tests++;                 \
        if (!(expr)) FAIL(#expr, __LINE__); \
    } while (0)

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))
#define RUN_TEST(test)        \
    do {                        \
        const char *msg = test(); \
        if (msg) return msg;      \
    } while (0)

static int static_num_tests = 0;

static void printBounds(DIFCodecBounds b) {
    printf("bounds{%d, %d, maxwidth: %d, maxheight: %d}", b.x, b.y, b.maxwidth, b.maxheight);
}

static bool checkBoundsEqual(DIFCodecBounds got, DIFCodecBounds want) {
    if (want.x != got.x || want.y != got.y || want.maxwidth != got.maxwidth || want.maxheight != got.maxheight) {
        printf("bounds are not equal; want: ");
        printBounds(want);
        printf(", got:");
        printBounds(got);
        printf("\n");
        return false;
    }
    return true;
}

static const char *test_partitioner(void) {
    DIFCodecPartitioner pr{12, 12};

    // 1
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=0,.y=0,.maxwidth=12,.maxheight=12}));

    ASSERT(pr.feedRectSize(5, 4) == true);

    // 2
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=5,.y=0,.maxwidth=7,.maxheight=12}));

    ASSERT(pr.feedRectSize(3, 1) == true);

    // 3
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=8,.y=0,.maxwidth=4,.maxheight=12}));

    ASSERT(pr.feedRectSize(1, 2) == true);

    // 4
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=9,.y=0,.maxwidth=3,.maxheight=12}));

    // try wrong things
    ASSERT(pr.feedRectSize(3, 0) == false);
    ASSERT(pr.feedRectSize(0, 8) == false);
    ASSERT(pr.feedRectSize(3, 13) == false);
    ASSERT(pr.feedRectSize(4, 8) == false);

    // try the right thing
    ASSERT(pr.feedRectSize(3, 8) == true);

    // 5
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=5,.y=1,.maxwidth=3,.maxheight=11}));

    ASSERT(pr.feedRectSize(1, 2) == true);

    // 6
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=6,.y=1,.maxwidth=2,.maxheight=11}));

    ASSERT(pr.feedRectSize(2, 11) == true);

    // 7
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=8,.y=2,.maxwidth=1,.maxheight=10}));

    ASSERT(pr.feedRectSize(1, 10) == true);

    // 8
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=5,.y=3,.maxwidth=1,.maxheight=9}));

    ASSERT(pr.feedRectSize(1, 9) == true);

    // 9
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=0,.y=4,.maxwidth=5,.maxheight=8}));

    ASSERT(pr.feedRectSize(4, 8) == true);

    // 10
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=4,.y=4,.maxwidth=1,.maxheight=8}));

    ASSERT(pr.feedRectSize(1, 8) == true);

    // 11
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=9,.y=8,.maxwidth=3,.maxheight=4}));

    ASSERT(pr.feedRectSize(1, 3) == true);

    // 12
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=10,.y=8,.maxwidth=2,.maxheight=4}));

    ASSERT(pr.feedRectSize(2, 4) == true);

    // 13
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=9,.y=11,.maxwidth=1,.maxheight=1}));

    ASSERT(pr.feedRectSize(1, 1) == true);

    ASSERT(pr.isFinished() == true);

    return NULL;
}

static const char *test_partitioner2(void) {
    DIFCodecPartitioner pr{6, 6};

    // 1
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=0,.y=0,.maxwidth=6,.maxheight=6}));

    ASSERT(pr.feedRectSize(2, 4) == true);

    // 2
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=2,.y=0,.maxwidth=4,.maxheight=6}));

    ASSERT(pr.feedRectSize(2, 3) == true);

    // 3
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=4,.y=0,.maxwidth=2,.maxheight=6}));

    ASSERT(pr.feedRectSize(2, 4) == true);

    // 4
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=2,.y=3,.maxwidth=2,.maxheight=3}));

    ASSERT(pr.feedRectSize(2, 1) == true);

    // 5
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=0,.y=4,.maxwidth=6,.maxheight=2}));

    ASSERT(pr.feedRectSize(6, 2) == true);

    ASSERT(pr.isFinished() == true);

    return NULL;
}

static const char *test_partitioner3(void) {
    DIFCodecPartitioner pr{10, 7};

    // 1
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=0,.y=0,.maxwidth=10,.maxheight=7}));

    ASSERT(pr.feedRectSize(2, 3) == true);

    // 2
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=2,.y=0,.maxwidth=8,.maxheight=7}));

    ASSERT(pr.feedRectSize(2, 4) == true);

    // 3
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=4,.y=0,.maxwidth=6,.maxheight=7}));

    ASSERT(pr.feedRectSize(2, 3) == true);

    // 4
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=6,.y=0,.maxwidth=4,.maxheight=7}));

    ASSERT(pr.feedRectSize(2, 6) == true);

    // 5
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=8,.y=0,.maxwidth=2,.maxheight=7}));

    ASSERT(pr.feedRectSize(2, 5) == true);



    // 6
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=0,.y=3,.maxwidth=2,.maxheight=4}));

    ASSERT(pr.feedRectSize(2, 2) == true);

    // 7
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=4,.y=3,.maxwidth=2,.maxheight=4}));

    ASSERT(pr.feedRectSize(2, 2) == true);

    // 8
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=2,.y=4,.maxwidth=2,.maxheight=3}));

    ASSERT(pr.feedRectSize(2, 1) == true);

    // 9
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=0,.y=5,.maxwidth=6,.maxheight=2}));

    ASSERT(pr.feedRectSize(6, 2) == true);

    // 10
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=8,.y=5,.maxwidth=2,.maxheight=2}));

    ASSERT(pr.feedRectSize(2, 2) == true);

    // 11
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=6,.y=6,.maxwidth=2,.maxheight=1}));

    ASSERT(pr.feedRectSize(2, 1) == true);

    ASSERT(pr.isFinished() == true);

    return NULL;
}

static const char *test_partitioner4(void) {
    DIFCodecPartitioner pr{10, 6};

    // 1
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=0,.y=0,.maxwidth=10,.maxheight=6}));

    ASSERT(pr.feedRectSize(2, 3) == true);

    // 2
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=2,.y=0,.maxwidth=8,.maxheight=6}));

    ASSERT(pr.feedRectSize(2, 4) == true);

    // 3
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=4,.y=0,.maxwidth=6,.maxheight=6}));

    ASSERT(pr.feedRectSize(2, 3) == true);

    // 4
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=6,.y=0,.maxwidth=4,.maxheight=6}));

    ASSERT(pr.feedRectSize(2, 6) == true);

    // 5
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=8,.y=0,.maxwidth=2,.maxheight=6}));

    ASSERT(pr.feedRectSize(2, 5) == true);



    // 6
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=0,.y=3,.maxwidth=2,.maxheight=3}));

    ASSERT(pr.feedRectSize(2, 2) == true);

    // 7
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=4,.y=3,.maxwidth=2,.maxheight=3}));

    ASSERT(pr.feedRectSize(2, 2) == true);

    // 8
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=2,.y=4,.maxwidth=2,.maxheight=2}));

    ASSERT(pr.feedRectSize(2, 1) == true);

    // 9
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=0,.y=5,.maxwidth=6,.maxheight=1}));

    ASSERT(pr.feedRectSize(6, 1) == true);

    // 10
    ASSERT(pr.isFinished() == false);
    ASSERT(checkBoundsEqual(pr.getNextBounds(),
                DIFCodecBounds{.x=8,.y=5,.maxwidth=2,.maxheight=1}));

    ASSERT(pr.feedRectSize(2, 1) == true);

    ASSERT(pr.isFinished() == true);

    return NULL;
}

static const char *run_all_tests(void) {
    RUN_TEST(test_partitioner);
    RUN_TEST(test_partitioner2);
    RUN_TEST(test_partitioner3);
    RUN_TEST(test_partitioner4);
    return NULL;
}

int main(void) {
    const char *fail_msg = run_all_tests();
    printf("%s, tests run: %d\n", fail_msg ? "FAIL" : "PASS", static_num_tests);
    return fail_msg == NULL ? 0 : 1;
}
