COMPRESSION EXERCISE

The goal of this exercise is to create a simple LOSSY image compression format. You will need to finish two programs: encoder.cpp and decoder.cpp. 

Encoder:
`encoder input.png output.bin` - Where the input file is a PNG and the output is your compression format

Decoder:
`decoder input.bin output.png` - Where the input file is your custom compression format and the output is a PNG file

We are using PNG for the source and final output so we can easily view the results of the compression. Otherwise we'd also have to write a viewer for the custom compression format :)

To get you started:
- lodepng.h is included and already integrated for reading/writing PNG files
- Two Xcode projects are included
- If you prefer, a Makefile is also included

To get started, open up encoder.cpp and decoder.cpp and get coding!

REQUIREMENTS:

- Written in C or C++
- Code will run on Mac or Linux
- The size of the compressed output is smaller than the raw image size (in this case 3MB). It does NOT have to be smaller than the PNG file
- The compression algorithm must be written by you, so you can't just plug in libjpeg :)
- What compression ratio is required? That's for you to decide


RAW IMAGE SIZE

The raw image size for the test.png is: width * height * bytes per pixel
Which for this example is: 1024 * 768 * 4 = 3,145,728 or 3MB
So your compression format needs to be under 3MB on the test image

Good luck!
