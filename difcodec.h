/*
 * Dumb Image Format (DIF) codec.
 *
 * The main idea is pretty simple: detect rectangles with the color which is
 * considered similar enough, and only store sizes of the rectangles (as
 * varints), and their colors. The coords of each rectangle can always be
 * deduced from the image data which has already been processed (encoded or
 * decoded), so we need to store only size, not coords. Most of the rectangles
 * are less than 127x127 pixels, so, each rectangle typically takes 5 bytes:
 * Width, Height, R, G, B. Alpha channel is not used in this codec.
 *
 * Possible improvements which could be done:
 *
 * - Instead of saving 3 RGB bytes for each rectangle, we could have a palette,
 *   and store a varint index for each rectangle. For images with a few colors
 *   (up to 127) that would provide almost 40% of compression improvement! For
 *   images with more colors, the improvement will be typically not less than
 *   ~20%. And the most used colors should still be placed in the beginning of
 *   the palette, so that at least 127 most used colors will take just 1 byte.
 *
 * - Given the fact that most rectangles are much smaller than 127x127 pixels, 
 *   we could adjust the varint algorithm a bit: instead of using 7th bit for
 *   the first "continuation flag", we could use a smaller number, which we
 *   can vary depending on the particular image data. This also poses an
 *   additional challenge of handling a continuous bits flow instead of
 *   standalone bytes, but that's not a rocket science either. For images with
 *   lots of small rectangles this change could provide up to 20% of
 *   compression improvement.
 *
 * ----------
 *
 * There is a "color tolerance" parameter: colors whose numeric difference is
 * less than the tolerance value, will be considered the same. The difference
 * between two colors is calculated as follows: differences of all channels (R,
 * G, B) are calculated separately (each channel value is from 0 to 255), and
 * then summed up. Default tolerance value is 50.
 *
 * == Decuction of the next rectangle coordinates ==
 *
 * There is a class DIFCodecPartitioner for that, it's used for both encoding
 * and decoding. Instances have three methods:
 *
 *   - isFinished(): returns whether the image has been fully processed;
 *   - getNextBounds(): returns X, Y coords of the next rectangle, and its
 *     maximum size;
 *   - feedRectSize(width, height): commits the next rectangle size
 *
 * getNextBounds() always returns the coordinates of the free rectangle with
 * minimal Y and X (Y is considered first).  It's implemented in an easy and
 * efficient way: there are two priority queues (on top of a binary tree) of
 * future rectangle coordinates; the first queue is ordered by Y, X, and the
 * second one is ordered by X, Y. These queues contain the same data; only
 * ordering differs.
 *
 * Initially, it only contains one coordinates: (0, 0), so, this is what
 * getNextBounds() returns when called for the first time; and max size is, of
 * course, the whole image size. E.g. given the image size 10x10, max width
 * will be 10x10:
 *
 *       0 1 2 3 4 5 6 7 8 9
 *      +--------------------+
 *    0 |. . . . . . . . . . |
 *    1 |. . . . . . . . . . |
 *    2 |. . . . . . . . . . |
 *    3 |. . . . . . . . . . |
 *    4 |. . . . . . . . . . |
 *    5 |. . . . . . . . . . |
 *    6 |. . . . . . . . . . |
 *    7 |. . . . . . . . . . |
 *    8 |. . . . . . . . . . |
 *    9 |. . . . . . . . . . |
 *      +--------------------+
 *
 * Now, assume we call feedRectSize() with (5, 7). It pops the current top of
 * the queue (0, 0), and adds 1 or 2 new items (depending of whether the righ
 * edge was reached); in this case, (0, 7) and (5, 0) will be added.
 *
 *       0 1 2 3 4 5 6 7 8 9
 *      +--------------------+
 *    0 |X X X X X . . . . . |
 *    1 |X X X X X . . . . . |
 *    2 |X X X X X . . . . . |
 *    3 |X X X X X . . . . . |
 *    4 |X X X X X . . . . . |
 *    5 |X X X X X . . . . . |
 *    6 |X X X X X . . . . . |
 *    7 |. . . . . . . . . . |
 *    8 |. . . . . . . . . . |
 *    9 |. . . . . . . . . . |
 *      +--------------------+
 *
 * The top of the queue is (5, 0) now, since this is the point with the minimum
 * Y coord. So, getNextBounds() returns (5, 0) with the max size 5x10.
 *
 * Now we call feedRectSize() with (3, 2). So, (5, 0) is popped, and 2 new
 * items added: (8, 0) and (5, 2).
 *
 *       0 1 2 3 4 5 6 7 8 9
 *      +--------------------+
 *    0 |X X X X X X X X . . |
 *    1 |X X X X X X X X . . |
 *    2 |X X X X X . . . . . |
 *    3 |X X X X X . . . . . |
 *    4 |X X X X X . . . . . |
 *    5 |X X X X X . . . . . |
 *    6 |X X X X X . . . . . |
 *    7 |. . . . . . . . . . |
 *    8 |. . . . . . . . . . |
 *    9 |. . . . . . . . . . |
 *      +--------------------+
 *
 * getNextBounds() now returns (8, 0) with the max size 2x10. We call
 * feedRectSize() with (2, 4). So, (8, 0) is popped, only one new item added,
 * because there's no free space to the right: (8, 4).
 *
 *       0 1 2 3 4 5 6 7 8 9
 *      +--------------------+
 *    0 |X X X X X X X X X X |
 *    1 |X X X X X X X X X X |
 *    2 |X X X X X . . . X X |
 *    3 |X X X X X . . . X X |
 *    4 |X X X X X . . . . . |
 *    5 |X X X X X . . . . . |
 *    6 |X X X X X . . . . . |
 *    7 |. . . . . . . . . . |
 *    8 |. . . . . . . . . . |
 *    9 |. . . . . . . . . . |
 *      +--------------------+
 *
 * getNextBounds() now returns (5, 2) with the max size 3x8.
 *
 * And so forth.
 *
 * Next rectangle coordinates are always the top of the first priority queue
 * (which considers Y first). The second queue (by X) is needed to get max
 * width of each rectangle: e.g. right now, after we pick (5, 2) from the top
 * of the first queue, we get the item after (5, 2) from the second queue , in
 * this case it's (8, 4), and thus we can know that the maximum width is 8-5,
 * i.e. 3.
 */

#include <cstdint>
#include <cstdlib>
#include <set>
#include <vector>

#ifndef _DIFCODEC_H
#define _DIFCODEC_H

#define BYTES_PER_PIXEL 4
#define DEFAULT_COLOR_TOLERANCE 50

class DIFCodec {
public:
    DIFCodec(int colorTolerance = DEFAULT_COLOR_TOLERANCE) :
        tolerance(colorTolerance)
    {
        if (tolerance < 0) {
            tolerance = DEFAULT_COLOR_TOLERANCE;
        }
    }

    /*
     * Encodes the raw image data (which is RGBARGBA....), and returns a vector
     * with the compressed data.
     */
    std::vector<uint8_t> encode(
            const std::vector<uint8_t> &rawdata,
            uint32_t width, uint32_t height, bool *ok
            );
    /*
     * Decodes the DIF data and returns a vector with the raw data (RGBARGBA...).
     */
    std::vector<uint8_t> decode(
            const std::vector<uint8_t> &comprdata,
            uint32_t *width, uint32_t *height, bool *ok
            );

private:
    bool putRect(
            std::vector<uint8_t>::iterator imgbegin,
            uint32_t imgwidth, uint32_t imgheight,
            uint32_t x, uint32_t y, uint32_t width, uint32_t height,
            uint32_t color
            );

    inline size_t getPixOffset(uint32_t imgwidth, uint32_t x, uint32_t y) {
        return (x + y * imgwidth) * BYTES_PER_PIXEL;
    }

    inline uint32_t getColor(
            const std::vector<uint8_t> &rawdata, uint32_t imgwidth,
            uint32_t x, uint32_t y, uint8_t *r, uint8_t *g, uint8_t *b, uint8_t *a
            ) {
        uint32_t color = 0;
        auto p = getPixOffset(imgwidth, x, y);
        color |= (uint32_t)(*r = rawdata[p + 0]) << 24;
        color |= (uint32_t)(*g = rawdata[p + 1]) << 16;
        color |= (uint32_t)(*b = rawdata[p + 2]) << 8;
        color |= (uint32_t)(*a = rawdata[p + 3]) << 0;
        return color;
    }

    inline int getColorChannelDiff(
            uint8_t a, uint8_t b
            ) {
        return a > b ? a - b : b - a;
    }

    inline bool isColorEqual(uint32_t color1, uint32_t color2);

    int tolerance;
};

struct DIFCodecBounds {
    uint32_t x;
    uint32_t y;
    uint32_t maxwidth;
    uint32_t maxheight;
};

// DIFCodecPoint {{{
/*
 * Just a structure with x, y coordinates; it's used as a base class for
 * a couple of subclasses which have different ordering policy: by Y or by X.
 */
class DIFCodecPoint {
public:
    DIFCodecPoint(uint32_t x, uint32_t y) :
        x(x),
        y(y)
    {}

public:
    uint32_t x;
    uint32_t y;
};

class DIFCodecPointByY : public DIFCodecPoint {
public:
    DIFCodecPointByY(uint32_t x, uint32_t y) : DIFCodecPoint(x, y) {}

    bool operator<(const DIFCodecPointByY &rhs) const {
        return y < rhs.y || (y == rhs.y && x < rhs.x);
    }
};

class DIFCodecPointByX : public DIFCodecPoint {
public:
    DIFCodecPointByX(uint32_t x, uint32_t y) : DIFCodecPoint(x, y) {}

    bool operator<(const DIFCodecPointByX &rhs) const {
        return x < rhs.x || (x == rhs.x && y < rhs.y);
    }
};
// }}}

class DIFCodecPartitioner {
public:
    DIFCodecPartitioner(uint32_t width, uint32_t height) :
        freeAreasByY(),
        freeAreasByX(),
        imgwidth(width),
        imgheight(height)
    {
        insertFreeArea(0, 0);
    }

    /*
     * Returns whether the image is filled
     */
    bool isFinished() const;

    /*
     * Returns the next rectangle area. Subsequent calls return the same
     * result, until the next call to feedRectSize().
     */
    DIFCodecBounds getNextBounds();

    /*
     * Commit rectangle size (starting point of the rectangle is always assumed
     * to be the one returned from the previous call to getNextBounds())
     */
    bool feedRectSize(uint32_t width, uint32_t height);

private:
    uint32_t imgwidth;
    uint32_t imgheight;

    /*
     * Starting points of free areas; we need both orderings: "by X" and "by
     * Y", so we have two separate sets which contain the same items. See
     * detailed explanation at the top of this file.
     */
    std::set<DIFCodecPointByY> freeAreasByY;
    std::set<DIFCodecPointByX> freeAreasByX;

    void insertFreeArea(uint32_t x, uint32_t y);
    void eraseFreeArea(uint32_t x, uint32_t y);
};

#endif // _DIFCODEC_H
