
#include "difcodec.h"
#include "varint.h"
#include <iterator>
#include <iostream>
#include <numeric>

#include <cstdlib>
#include <ctime>

using namespace std;

// DIFCodec {{{

vector<uint8_t> DIFCodec::encode(
        const vector<uint8_t> &rawdata, uint32_t width, uint32_t height, bool *ok
        ) {
    std::srand(std::time(0));
    vector<uint8_t> comprdata;
    auto inserter = back_inserter(comprdata);

    // Write width and height of the image
    encodeVarint(width, inserter);
    encodeVarint(height, inserter);

    DIFCodecPartitioner pr{width, height};

    // Iterate until we process the whole image
    while (!pr.isFinished()) {
        // Get the next bounds of the next rectangle to process
        DIFCodecBounds bounds = pr.getNextBounds();

        // Create buffers for all channels (RGBA), we need them for the average
        // color calculation
        vector<uint8_t> rbuf;
        vector<uint8_t> gbuf;
        vector<uint8_t> bbuf;
        vector<uint8_t> abuf;

        // Get color of the starting rectangle point
        uint32_t colorStart;
        {
            uint8_t r, g, b, a;
            colorStart = getColor(rawdata, width, bounds.x, bounds.y, &r, &g, &b, &a);
            rbuf.push_back(r);
            gbuf.push_back(g);
            bbuf.push_back(b);
            abuf.push_back(a);
        }

        // Check each point of the rectangle, until we find the "different"
        // color or reach the bound on each direction
        int ystep = 1;
        int xstep = 1;
        uint32_t ylim;
        uint32_t xlim;
        for (
                xlim = bounds.x + 1, ylim = bounds.y + 1;
                ystep != 0 || xstep != 0;
                xlim += xstep, ylim += ystep
            ) {

            // Make sure we won't get out of the bounds returned by
            // getNextBounds()
            if (xstep != 0 && xlim >= bounds.x + bounds.maxwidth) {
                xlim = bounds.x + bounds.maxwidth - 1;
                xstep = 0;
            }
            if (ystep != 0 && ylim >= bounds.y + bounds.maxheight) {
                ylim = bounds.y + bounds.maxheight - 1;
                ystep = 0;
            }

            uint32_t yc = 0;
            uint32_t xc = 0;

            // Check the right side, if needed
            if (xstep != 0) {
                for (yc = bounds.y; yc <= ylim; yc++) {
                    uint8_t r, g, b, a;
                    uint32_t color = getColor(rawdata, width, xlim, yc, &r, &g, &b, &a);
                    if (!isColorEqual(color, colorStart)) {
                        xlim -= 1;
                        xstep = 0;
                        break;
                    }
                    rbuf.push_back(r);
                    gbuf.push_back(g);
                    bbuf.push_back(b);
                    abuf.push_back(a);
                }
            }

            // Check the bottom side, if needed
            if (ystep != 0) {
                for (xc = bounds.x; xc <= xlim; xc++) {
                    uint8_t r, g, b, a;
                    uint32_t color = getColor(rawdata, width, xc, ylim, &r, &g, &b, &a);
                    if (!isColorEqual(color, colorStart)) {
                        ylim -= 1;
                        ystep = 0;
                        break;
                    }
                    rbuf.push_back(r);
                    gbuf.push_back(g);
                    bbuf.push_back(b);
                    abuf.push_back(a);
                }
            }
        }

        // Now we have the size of the next rectangle
        uint32_t rectWidth = xlim - bounds.x + 1;
        uint32_t rectHeight = ylim - bounds.y + 1;

        // Feed it to the partitioner
        pr.feedRectSize(rectWidth, rectHeight);

        // Calculate color of the current rectangle, which is normally an
        // average of all rectangle points.
        //uint32_t colorAvg = std::rand() % 0xffffffff + 1;
        uint32_t colorAvg = 0
            | ((std::accumulate(rbuf.begin(), rbuf.end(), 0) / rbuf.size()) << 24)
            | ((std::accumulate(gbuf.begin(), gbuf.end(), 0) / gbuf.size()) << 16)
            | ((std::accumulate(bbuf.begin(), bbuf.end(), 0) / bbuf.size()) << 8)
            | ((std::accumulate(abuf.begin(), abuf.end(), 0) / abuf.size()) << 0)
            ;

        // Encode image data
        encodeVarint(rectWidth, inserter);
        encodeVarint(rectHeight, inserter);
        inserter = (colorAvg >> 24) & 0xff;
        inserter = (colorAvg >> 16) & 0xff;
        inserter = (colorAvg >> 8)  & 0xff;
    }

    *ok = true;
    return comprdata;
}

vector<uint8_t> DIFCodec::decode(
        const vector<uint8_t> &comprdata, uint32_t *pwidth, uint32_t *pheight,
        bool *ok
        ) {
    vector<uint8_t> rawdata;
    auto it = comprdata.begin();
    auto end = comprdata.end();

    // Read width and height of the image
    *pwidth = decodeVarint(&it, end);
    *pheight = decodeVarint(&it, end);

    auto imgbegin_idx = rawdata.size();
    rawdata.resize(imgbegin_idx + *pwidth * *pheight * BYTES_PER_PIXEL);
    auto imgbegin = rawdata.begin() + imgbegin_idx;

    DIFCodecPartitioner pr{*pwidth, *pheight};

    // Iterate until we process the whole image
    while (!pr.isFinished() && it != end) {
        DIFCodecBounds bounds = pr.getNextBounds();
        auto width = decodeVarint(&it, end);
        auto height = decodeVarint(&it, end);
        uint32_t color = 0xff;
        if (it + 3 /*RGB*/ > end) {
            cerr << "Failed to decode image: insufficient data" << endl;
            *ok = false;
            return rawdata;
        }
        color |= (*it++ << 24);
        color |= (*it++ << 16);
        color |= (*it++ << 8);

        if (!putRect(imgbegin, *pwidth, *pheight, bounds.x, bounds.y, width, height, color)) {
            cerr << "Failed to decode image" << endl;
            *ok = false;
            return rawdata;
        }
        pr.feedRectSize(width, height);
    }

    *ok = true;
    return rawdata;
}

bool DIFCodec::putRect(
        std::vector<uint8_t>::iterator imgbegin, uint32_t imgwidth, uint32_t imgheight,
        uint32_t x, uint32_t y, uint32_t width, uint32_t height,
        uint32_t color
        ) {
    // Make sure we're not going to draw out of the image bounds
    if (x + width > imgwidth) {
        cerr << "putRect outside of the image: "
            << "x: " << x << "; "
            << "width: " << width << "; "
            << "imgwidth: " << imgwidth << "."
            << endl;
        return false;
    }

    if (y + height > imgheight) {
        cerr << "putRect outside of the image: "
            << "y: " << y << "; "
            << "height: " << height << "; "
            << "imgheight: " << imgheight << "."
            << endl;
        return false;
    }

    // Put image data
    for (uint32_t cy = y; cy < y + height; cy++) {
        for (uint32_t cx = x; cx < x + width; cx++) {
            auto p = imgbegin + getPixOffset(imgwidth, cx, cy);
            *(p + 0) = (color & 0xff000000L) >> 24;
            *(p + 1) = (color & 0x00ff0000L) >> 16;
            *(p + 2) = (color & 0x0000ff00L) >> 8;
            *(p + 3) = (color & 0x000000ffL) >> 0;
        }
    }
    return true;
}

inline bool DIFCodec::isColorEqual(uint32_t color1, uint32_t color2) {
    int diff = 0
        + getColorChannelDiff((color1 >> 24) & 0xff, (color2 >> 24) & 0xff)
        + getColorChannelDiff((color1 >> 16) & 0xff, (color2 >> 16) & 0xff)
        + getColorChannelDiff((color1 >> 8)  & 0xff, (color2 >> 8)  & 0xff)
        // we ignore Alpha channel in this codec
        //+ getColorChannelDiff((color1 >> 0)  & 0xff, (color2 >> 0)  & 0xff)
        ;
    return diff <= tolerance;
}

// }}}

// DIFCodecPartitioner {{{

bool DIFCodecPartitioner::isFinished() const {
    auto itlow = freeAreasByY.lower_bound(DIFCodecPointByY(0, 0));
    return (*itlow).y >= imgheight;
}

DIFCodecBounds DIFCodecPartitioner::getNextBounds() {
    if (isFinished()) {
        return DIFCodecBounds{};
    }

    // Get the top of the priority queue by Y; its coords will be returned.
    auto itlow = freeAreasByY.lower_bound(DIFCodecPointByY(0, 0));
    auto x = (*itlow).x;
    auto y = (*itlow).y;

    // There might be more than one adjacent rectangle coordinates which have
    // the same Y value; we need to eliminate all of them (only the leftmost
    // one should be used). For that, we iterate both queues from the current
    // point: for adjacent items, they both return the same coords.
    for(;;) {
        auto itupY = freeAreasByY.upper_bound(DIFCodecPointByY((*itlow).x, (*itlow).y));
        auto itupX = freeAreasByX.upper_bound(DIFCodecPointByX((*itlow).x, (*itlow).y));

        if (       itupY != freeAreasByY.end()
                && itupX != freeAreasByX.end()
                && (*itlow).y == (*itupY).y
                && (*itupY).x == (*itupX).x
                && (*itupY).y == (*itupX).y
           )
        {
            // Remove extra rectangle coord
#if DIF_VERBOSE >= 3
            printf("Removing extra free area: (%d, %d)\n", (*itupY).x, (*itupY).y);
#endif
            freeAreasByY.erase(itupY);
            freeAreasByX.erase(itupX);
        } else {
            break;
        }
    }

    // Determine the max width of the rectangle to be returned
    auto maxwidth = imgwidth - x;
    auto itupX = freeAreasByX.upper_bound(DIFCodecPointByX(x, y));
    if (itupX != freeAreasByX.end()) {
        maxwidth = (*itupX).x - x;
    }

    // Return the rectangle
    DIFCodecBounds bounds = {
        .x = x,
        .y = y,
        .maxwidth = maxwidth,
        .maxheight = imgheight - y,
    };
    return bounds;
}

bool DIFCodecPartitioner::feedRectSize(uint32_t width, uint32_t height) {
    if (isFinished()) {
        return false;
    }

    // Get current bounds and check that the new rect size is within them
    auto curBounds = getNextBounds();

    if (width > curBounds.maxwidth) {
        cerr << "Feeding rect with too large width: " << width
            << ", maxwidth: " << curBounds.maxwidth << endl;
        return false;
    } else if (height > curBounds.maxheight) {
        cerr << "Feeding rect with too large width: " << height
            << ", maxheight: " << curBounds.maxheight << endl;
        return false;
    } else if (width == 0) {
        cerr << "Feeding rect with zero width" << endl;
        return false;
    } else if (height == 0) {
        cerr << "Feeding rect with zero height" << endl;
        return false;
    }

    // Erase current top item from the queue
    eraseFreeArea(0, 0);

    // Note: we add new "free area" even if it is out of the image bounds,
    // this is needed for the adjacent areas detection to work correctly on
    // the image boundary as well
    insertFreeArea(curBounds.x, curBounds.y + height);

    // Add new free area to the right of the newly fed rectangle, if only there
    // is some room to the right.
    if (width < curBounds.maxwidth) {
        insertFreeArea(curBounds.x + width, curBounds.y);
    }

#if DIF_VERBOSE >= 3
    {
        printf("fed new rect: (x=%d, y=%d, width=%d, height=%d), new queue:\n", curBounds.x, curBounds.y, width, height);
        auto cur = DIFCodecPointByY(0, 0);
        for (int i = 0; i < freeAreasByY.size(); i++) {
            auto itlow = i == 0 ? freeAreasByY.lower_bound(cur) : freeAreasByY.upper_bound(cur);
            printf("#%d: (%d, %d)\n", i, (*itlow).x, (*itlow).y);
            cur = *itlow;
        }
    }
#endif

    return true;
}

void DIFCodecPartitioner::insertFreeArea(uint32_t x, uint32_t y) {
    freeAreasByY.insert(DIFCodecPointByY(x, y));
    freeAreasByX.insert(DIFCodecPointByX(x, y));
}

void DIFCodecPartitioner::eraseFreeArea(uint32_t x, uint32_t y) {
    auto itlowY = freeAreasByY.lower_bound(DIFCodecPointByY(x, y));
    freeAreasByY.erase(itlowY);
    auto itlowX = freeAreasByX.lower_bound(DIFCodecPointByX((*itlowY).x, (*itlowY).y));
    freeAreasByX.erase(itlowX);
}

// }}}
