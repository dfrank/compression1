#include "lodepng.h"
#include "difcodec.h"
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <fstream>

bool decompress(const char* inputFilename, const char* outputFilename) {
    uint32_t width = 0;
    uint32_t height = 0;

    // Load compressed DIF data as a vector
    std::ifstream file(inputFilename, std::ios::binary | std::ios::ate);
    std::streamsize size = file.tellg();
    file.seekg(0, std::ios::beg);

    std::vector<uint8_t> buffer(size);
    if (!file.read((char *)buffer.data(), size)) {
        printf("failed to read from file");
        return false;
    }

    // Uncompress DIF data into a raw vector
    DIFCodec codec;
    bool ok;
    buffer = codec.decode(buffer, &width, &height, &ok);
    if (!ok) {
        return false;
    }

    // Write the pixel buffer out as a PNG so we can easily view the output
    unsigned int error = lodepng_encode32_file(
            outputFilename, (unsigned char *)buffer.data(), width, height
            );

    if (error) {
        printf("error %u: %s\n", error, lodepng_error_text(error));
        return false;
    }

    return true;
}


int main(int argc, char *argv[]) {
    const char* inputFilename = argc > 1 ? argv[1] : "output.bin";
    const char* outputFilename = argc > 2 ? argv[2] : "output.png";

    bool ok = decompress(inputFilename, outputFilename);
    if (!ok) {
        return 1;
    }
    return 0;
}
