/**
 * C++ utilities for variable-length integer encoding
 * Compile with -std=c++11 or higher
 *
 * Version 1.1: Use size_t for size argument, improve function signatures
 *
 * License: CC0 1.0 Universal
 * Originally published on http://techoverflow.net
 * Copyright (c) 2015 Uli Koehler
 * Modified by Dmitry Frank to support iterators
 */
#ifndef _VARINT_H
#define _VARINT_H
#include <cstdint>
#include <vector>
#include <iterator>

/*
 * Encodes given value as a varint. `it` should be an output iterator, e.g.
 * back_inserter. Returns the number of bytes occupied by the encoded varint.
 */
template<typename T = uint64_t, typename IterOut>
size_t encodeVarint(T value, IterOut it) {
    size_t outputSize = 0;
    // While more than 7 bits of data are left, occupy the last output byte
    // and set the next byte flag
    while (value > 0x7f) {
        // |0x80: Set the next byte flag
        it = ((uint8_t)(value & 0x7f)) | 0x80;

        // Remove the seven bits we just wrote
        value >>= 7;
        outputSize++;
    }
    it = ((uint8_t)value) & 0x7f;
    outputSize++;
    return outputSize;
}

/*
 * Decodes the varint from the iterator `first`, which is advanced by the
 * position after the decoded varint.
 *
 * TODO: indicate an error if varint wasn't fully decoded (i.e. if afterEnd
 * came earlier)
 */
template<typename T = uint64_t, typename Iter>
T decodeVarint(Iter *first, Iter afterEnd) {
    T ret = 0;
    int i = 0;
    Iter cur = *first;
    for (; cur != afterEnd; cur++, i++) {
        ret |= (*cur & 0x7f) << (7 * i);
        // If the next-byte flag is set
        if(!(*cur & 0x80)) {
            cur++;
            break;
        }
    }
    *first = cur;
    return ret;
}
#endif //_VARINT_H
